package eps.ua.es.pastalist;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    private ListView lista;
    private PastaAdapter pastaAdapter;
    public final static String URL_ELEM = "URL_ELEM";
    public final static String NAME_ELEM = "NAME_ELEM";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lista = (ListView)findViewById(R.id.list_main_view);
        pastaAdapter = new PastaAdapter(getApplicationContext(),PastaList.getPasta());
        lista.setAdapter(pastaAdapter);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                Pasta pasta = (Pasta)lista.getAdapter().getItem(position);
                intent.putExtra(URL_ELEM, pasta.getUrl());
                intent.putExtra(NAME_ELEM, pasta.getName());
                startActivity(intent);
            }
        });
    }
}
