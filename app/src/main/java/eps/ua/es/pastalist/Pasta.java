package eps.ua.es.pastalist;

import android.graphics.Bitmap;
import android.os.Parcelable;

import java.lang.ref.SoftReference;
import java.util.ArrayList;

public class Pasta extends ArrayList<Parcelable> {
    private String mUrl;
    private String mName;
    private SoftReference<Bitmap> mBitmap;

    Pasta( String _url, String _name ) {
        mUrl = _url;
        mName = _name;
        mBitmap = null;
    }

    public String getUrl() { return mUrl; }
    public void setUrl( String _url ) { mUrl = _url; }

    public String getName() { return mName; }
    public void setName( String _name ) { mName = _name; }

    public SoftReference<Bitmap> getBitmap() { return mBitmap; }
    public void setBitmap( SoftReference<Bitmap> _bitmap ) { mBitmap = _bitmap; }
}
