package eps.ua.es.pastalist;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.IOException;
import java.lang.ref.SoftReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class PastaLoader extends AsyncTask<Object, Void, Bitmap>
{
    ImageView mImageView = null;
    Pasta mPastaItem = null;

    @Override
    protected Bitmap doInBackground(Object... params)
    {
        mPastaItem = (Pasta) params[0];
        mImageView = (ImageView) params[1];

        return prv_imageLoader( mPastaItem.getUrl() );
    }

    @Override
    protected void onPostExecute (Bitmap result)
    {
        if (result != null)
        {
            mImageView.setImageBitmap(result);
            mPastaItem.setBitmap(new SoftReference<Bitmap>(result));
        }
    }

    private Bitmap prv_imageLoader(String url)
    {
        Bitmap bitmap = null;
        HttpURLConnection http = null;

        try {
            URL uri = new URL(url);
            http = (HttpURLConnection)uri.openConnection();

            if(http.getResponseCode() == HttpURLConnection.HTTP_OK){
                bitmap = BitmapFactory.decodeStream(http.getInputStream());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if( http != null )
                http.disconnect();
        }
        return bitmap;
    }

}
