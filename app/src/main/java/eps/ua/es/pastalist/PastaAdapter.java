package eps.ua.es.pastalist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PastaAdapter extends BaseAdapter implements AbsListView.OnScrollListener
{
    private List<Pasta> mList;
    private Context mContext;
    private Map<String, PastaLoader> mapDL;
    private Boolean mBusy = false;

    public PastaAdapter(Context context, List<Pasta> objects)
    {
        mContext = context;
        mList = objects;
        mapDL = new HashMap<String, PastaLoader>();
    }

    @Override
    public int getCount()
    {
        return mList.size();
    }

    @Override
    public Object getItem(int position)
    {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(convertView == null) {
            LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.list_item, null);
        }

        TextView tvTexto = (TextView) convertView.findViewById(R.id.textView);
        ImageView ivIcono = (ImageView) convertView.findViewById(R.id.imageView);

        Pasta pasta = mList.get(position);
        tvTexto.setText(pasta.getName());

        if(mapDL.get(pasta.getName())==null && !mBusy){
            PastaLoader ploader = new PastaLoader();
            mapDL.put(pasta.getName(),ploader);
            ploader.execute(pasta, ivIcono);
        }else{
            ivIcono.setImageBitmap(pasta.getBitmap().get());
        }

        return convertView;
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        switch(scrollState) {
            case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                mBusy = false;
                notifyDataSetChanged();
                break;
            case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
                mBusy = true;
                break;
            case AbsListView.OnScrollListener.SCROLL_STATE_FLING:
                mBusy = true;
                break;
        }
    }
}
