package eps.ua.es.pastalist;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by mastermoviles on 25/01/2017.
 */
public class DetailActivity extends AppCompatActivity {

    private String url;
    private String name;
    private ImageView imagePasta;
    private TextView namePasta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_main);
        imagePasta = (ImageView)findViewById(R.id.imagePasta);
        namePasta = (TextView)findViewById(R.id.textPasta);
        name = getIntent().getStringExtra(MainActivity.NAME_ELEM);
        url = getIntent().getStringExtra(MainActivity.URL_ELEM);
        Pasta pasta = new Pasta(url,name);
        namePasta.setText(name);
        new PastaLoader().execute(pasta, imagePasta);
    }
}
